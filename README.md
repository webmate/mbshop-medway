Installation
------------

* Run bash script start.sh (script create MySQL docker container),
* Import database from directory "db",
* Configure .env file if you need,
* Run command "composer install",
* Run command "bin/console server:run" (php is required),
* Run url "http://127.0.0.1:8000" in browser.