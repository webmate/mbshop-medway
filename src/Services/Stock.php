<?php


namespace App\Services;


use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;

class Stock
{
    private $em;

    private $productRepository;

    public function __construct(EntityManagerInterface $em, ProductRepository $productRepository)
    {
        $this->em = $em;
        $this->productRepository = $productRepository;
    }

    public function decrease(array $cart)
    {
        foreach ($cart as $item) {
            $product = $this->productRepository->find($item['product']->getId());
            $currentCounter = $product->getStockCounter() - $item['count'];
            $product->setStockCounter($currentCounter);
            $this->em->flush();
        }
    }
}