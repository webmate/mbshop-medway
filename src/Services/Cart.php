<?php


namespace App\Services;


use App\Entity\User;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class Cart
{
    private $requestStack;

    private $productRepository;

    private $session;

    private $mailer;

    private $tokenStorage;

    private $stock;

    public function __construct(RequestStack $requestStack, ProductRepository $productRepository, SessionInterface $session, Mailer $mailer, TokenStorageInterface $tokenStorage, Stock $stock)
    {
        $this->requestStack = $requestStack;
        $this->productRepository = $productRepository;
        $this->session = $session;
        $this->mailer = $mailer;
        $this->tokenStorage = $tokenStorage;
        $this->stock = $stock;
    }

    public function add(): bool
    {
        $request = $this->requestStack->getCurrentRequest();
        $productId = $request->get('id');
        $product = $this->productRepository->find($productId);
        $session = $this->session;
        if (!$product) {
            $session->getFlashBag()->add('notice', 'Brak tego produktu.');
            return false;
        }
        $productCount = $request->get('count');

        if (0 == $productCount) {
            $session->getFlashBag()->add('notice', 'Podano nieprawidłową ilosć.');
            return false;
        }

        if ($product->getStockCounter() < $productCount) {
            $session->getFlashBag()->add('notice', 'Brak odpowiedniej ilości produktu.');
            return false;
        }

        $cart = $this->session->get('cart') ?? [];
        foreach ($cart as $item) {
            if ($product == $item['product']) {
                $session->getFlashBag()->add('notice', 'Posiadasz już ten produkt w koszyku.');
                return false;
            }
        }

        $session->getFlashBag()->add('success', 'Produkt został dodany do koszyka.');

        $cart[] = [
            'product' => $product,
            'count' => $productCount
        ];

        $session->set('cart', $cart);

        return true;
    }

    public function get(): array
    {
        return $this->session->get('cart') ?? [];
    }

    public function remove(int $productId): bool
    {
        $session = $this->session;
        $cart = $session->get('cart') ?? [];
        foreach ($cart as $key => &$item) {
            if ($item['product']->getId() == $productId) {
                unset($cart[$key]);
            }
        }

        $session->set('cart', $cart);
        $session->getFlashBag()->add('success', 'Produkt został usunięty z koszyka.');
        return true;
    }

    public function checkout(): bool
    {
        $session = $this->session;
        /**
         * @var $user User
         */
        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user) {
            $session->getFlashBag()->add('notice', 'Nie jesteś zalogowany.');
            return false;
        }
        $cart = $session->get('cart') ?? [];
        if (empty($cart)) {
            $session->getFlashBag()->add('notice', 'Twój koszyk jest pusty.');
            return false;
        }


        $this->mailer->sendOrder($user->getEmail(), $session->get('cart'));
        $this->stock->decrease($cart);
        $this->clear();
        $session->getFlashBag()->add('success', 'Twój zamówienie zostało przesłane do realizacji. Otrzymasz potwierdzenie na e-maila.');
        return true;
    }

    private function clear()
    {
        $this->session->set('cart', null);
    }
}