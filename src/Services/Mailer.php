<?php


namespace App\Services;


use Twig\Environment;

class Mailer
{

    private $mailer;

    private $twig;

    public function __construct(\Swift_Mailer $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function sendOrder(string $email, array $cart): bool
    {
        $message = new \Swift_Message();
        $message->setSubject('MBShop - Zamówienie');
        $message->setFrom('no-reply@webmate.ovh', 'MBShop');
        $message->setTo($email);
        $message->setBody(
            $this->twig->render('Email/order.html.twig', ['cart' => $cart]),
            'text/html'
        );

        $this->mailer->send($message);

        return true;
    }
}