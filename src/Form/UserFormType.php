<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'E-mail'
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Hasła muszą być takie same.',
                'first_options' => array('label' => 'Hasło'),
                'second_options' => array('label' => 'Powtórz hasło'),
            ))
            ->add('phone', null, [
                'label' => 'Numer telefonu(format: +48XXXXXXXXX)'
            ])
            ->add('name', null, [
                'label' => 'Imię'
            ])
            ->add('surname', null, [
                'label' => 'Nazwisko'
            ])
            ->add('city', null, [
                'label' => 'Miasto'
            ])
            ->add('street', null, [
                'label' => 'Ulica'
            ])
            ->add('streetNumber', null, [
                'label' => 'Numer budynku/lokalu'
            ])
            ->add('zipCode', null, [
                'label' => 'Kod pocztowy'
            ])
            ->add('nip', null, [
                'label' => 'NIP'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Zarejestruj'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
