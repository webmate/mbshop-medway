<?php


namespace App\Controller;


use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home_index")
     *
     */
    public function index(ProductRepository $productRepository, PaginatorInterface $paginator, Request $request)
    {
        $products = $productRepository->findPossibleToSell();
        return $this->render('Home/index.html.twig', [
            'products' => $paginator->paginate($products, $request->query->getInt('page', 1), 4)
        ]);
    }
}