<?php


namespace App\Controller;


use App\Services\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CartController
 * @package App\Controller
 * @Route("/cart")
 */
class CartController extends AbstractController
{
    /**
     * @Route("/add", name="app_cart_add", methods={"POST"})
     */
    public function add(Cart $cart)
    {
        $cart->add();

        return $this->redirectToRoute('app_home_index');
    }

    /**
     * @Route("/list", name="app_cart_list")
     */
    public function list(Cart $cart)
    {
        return $this->render('Cart/list.html.twig', [
            'cart' => $cart->get()
        ]);
    }

    /**
     * @Route("/remove/{productId}", name="app_cart_remove")
     */
    public function remove($productId, Cart $cart)
    {
        $cart->remove($productId);

        return $this->redirectToRoute('app_cart_list');
    }

    /**
     * @Route("/checkout", name="app_cart_checkout")
     */
    public function checkout(Cart $cart)
    {
        $cart->checkout();

        return $this->redirectToRoute('app_cart_list');
    }

    /**
     * @Route("/pdf", name="app_cart_pdf")
     */
    public function pdf(SessionInterface $session, ContainerInterface $container)
    {
        $cart = $session->get('cart') ?? [];
        $html = $this->renderView(
            'Cart/pdf.html.twig', [
                'cart' => $cart
            ]
        );
        $snappy = $container->get('knp_snappy.pdf');
        $filename = 'zamowienie';
        $options = [
            'margin-top'    => 10,
            'margin-right'  => 10,
            'margin-bottom' => 10,
            'margin-left'   => 10,
        ];
        return new Response(
            $snappy->getOutputFromHtml($html, $options),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . $filename . '.pdf"'
            )
        );
    }
}